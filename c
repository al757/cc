{
"wallpaper":"https://picsum.photos/1280/720/?blur=10",
"spider":"https://gitlab.com/al757/cc/-/raw/master/jar/4.jar",

## 02.🦋直播
"lives":[
{"group":"redirect","channels":[
{"name":"live","urls":["proxy://do=live&type=txt&ext=aHR0cHM6Ly9naXRsYWIuY29tL2FsNzU3L2NjLy0vcmF3L21hc3Rlci9sbC50eHQ"]}
]}
],

"sites":[
## 01.🐼缝合接口

{"key":"77_spider","name":"👒🐱七七(SP)","type":3,"api":"csp_Kunyu77","searchable":1,"quickSearch":1,"filterable":1},
{"key":"csp_Juzi","name":"🐱橘子(SP)","type":3,"api":"csp_Juzi","searchable":1,"quickSearch":1,"filterable":1},
{"key": "csp_Kuaikan","name": "🌀快看","type": 3,"api": "csp_Kuaikan","searchable": 1,"quickSearch": 1,"filterable": 1},
{"key":"csp_IKan","name":"💖爱看(SP)","type":3,"api":"csp_IKan","searchable":1,"quickSearch":0,"filterable":1},
{"key":"kmys_spider","name":"🐱酷猫(SP)","type":3,"api":"csp_Kmys","searchable":1,"quickSearch":1,"filterable":1},

{"key": "csp_Bttoo","name": "❤两个BT","type": 3,"api": "csp_Bttoo","searchable": 1,"quickSearch": 1,"filterable": 1},
{"key":"csp_CZSPP","name":"🐞厂长资源(SP)","type":3,"api":"csp_CZSPP","searchable":1,"quickSearch":1,"filterable":0},
{"key":"LiteApple","name":"🍎小苹果(SP)","type":3,"api":"csp_LiteApple","searchable":1,"quickSearch":1,"filterable": 1},
{"key":"csp_Zxzj","name":"🎈在线之家(SP)","type":3,"api":"csp_Zxzj","searchable":1,"quickSearch":1,"filterable":1,"ext":""},
{"key":"csp_Cokemv","name":"🐼Cokemv(SP)","type":3,"api":"csp_Cokemv","searchable":1,"quickSearch":1,"filterable":1,"ext":""},

{"key":"csp_DY1990","name":"🐼1990电影(SP)","type":3,"api":"csp_DY1990","searchable":1,"quickSearch":1,"filterable":1},
{"key":"csp_AliPanSou","name":"😻喵狸盘搜（仅支持搜索）","type":3,"api":"csp_AliPanSou","searchable":1,"quickSearch":1,"filterable":0},
{"key":"csp_xpath_Hdmoli","name":"🐼Hdmoli(XPF)","type":3,"api":"csp_XPathHdmoli","searchable":1,"quickSearch":1,"filterable":1,"ext":"https://gitlab.com/al757/cc/-/raw/master/XPath/hdmoli.json"},
{"key":"csp_Buka","name":"🐼真不卡(SP)","type":3,"api":"csp_Buka","searchable":1,"quickSearch":0,"filterable":1},
{"key":"csp_DiDuan","name":"🐼低端影视(SP)","type":3,"api":"csp_DiDuan","searchable":1,"quickSearch":1,"filterable":0},
{"key":"csp_GitCafe","name":"🦊小纸条(SP)","type":3,"api":"csp_GitCafe","searchable":1,"quickSearch":1,"filterable":0},

//{"key":"csp_Newfii","name":"🐼奈落影院(SP)","type":3,"api":"csp_Newfii","searchable":1,"quickSearch":1,"filterable":1,"ext":"clan://maoys/XPath/nailuoyy.json"},

{"key":"csp_BBB","name":"🅱Bili(SP)","type":3,"api":"csp_BBB","searchable":1,"quickSearch":1,"filterable":0},
{"key":"csp_Blidw","name":"🐘Bl动物世界(SP)","type":3,"api":"csp_Blidw","searchable":1,"quickSearch":1,"filterable":0},
{"key":"csp_Blixq","name":"🏮Bl戏曲(SP)","type":3,"api":"csp_Blixq","searchable":1,"quickSearch":1,"filterable":0},
{"key":"csp_Bliyc","name":"🎸Bl演唱(SP)","type":3,"api":"csp_Bliyc","searchable":1,"quickSearch":1,"filterable":0},
{"key":"csp_Blixs","name":"🎤相声小品(SP)","type":3,"api":"csp_Blixs","searchable":1,"quickSearch":1,"filterable":0},
{"key":"csp_Bliqx","name":"🏀Bl球星(SP)","type":3,"api":"csp_Bliqx","searchable":1,"quickSearch":1,"filterable":0},


##05.🥒(非官方)小黄瓜大佬 XBiubiu
{"key":"csp_biubiu_乐猪影视","name":"🥒乐猪影视(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/乐猪影视.json"},
{"key":"csp_biubiu_花猫TV","name":"🥒花猫TV(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/花猫TV.json"},
//{"key":"csp_biubiu_555资源","name":"🥒555资源(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/555资源.json"},
{"key":"csp_biubiu_口袋影院","name":"🥒口袋影院(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/口袋影院.json"},
{"key":"csp_biubiu_创艺影视","name":"🥒创艺影视(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/创艺影视.json"},
//{"key":"csp_biubiu_蓝光影视","name":"🥒蓝光影视(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/蓝光影视.json"},
//{"key":"csp_biubiu_在线之家","name":"🥒在线之家(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/在线之家.json"},
//{"key":"csp_biubiu_COKEMV","name":"🥒COKEMV(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/COKEMV.json"},
{"key":"csp_biubiu_饭团影院","name":"🥒饭团影院(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/饭团影院.json"},
{"key":"csp_biubiu_影视工厂","name":"🥒影视工厂(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/影视工厂.json"},
//{"key":"csp_biubiu_骚火电影","name":"🥒骚火电影(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/骚火电影.json"},
//{"key":"csp_biubiu_莫扎兔","name":"🥒莫扎兔(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/莫扎兔.json"},
//{"key":"csp_biubiu_完美看看影视","name":"🥒完美看看影视(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/完美看看影视.json"},
//{"key":"csp_biubiu_九州影视","name":"🥒九州影视(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/九州影视.json"},
//{"key":"csp_biubiu_VIP1280","name":"🥒VIP1280(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/VIP1280.json"},
//{"key":"csp_biubiu_厂长资源-蓝光","name":"🥒厂长资源-蓝光(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/厂长资源-蓝光.json"},
//{"key":"csp_biubiu_555电影","name":"🥒555电影(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/555电影.json"},
{"key":"csp_biubiu_思乐影视","name":"🥒思乐影视(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/思乐影视.json"},
{"key":"csp_biubiu_VIP电影院","name":"🥒VIP电影院(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/VIP电影院.json"},
{"key":"csp_biubiu_看一看影视","name":"🥒看一看影视(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/看一看影视.json"},
//{"key":"csp_biubiu_31看影视","name":"🥒31看影视(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/31看影视.json"},
{"key":"csp_biubiu_小强迷","name":"🥒小强迷(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/小强迷.json"},


{"key": "腾讯","name": "腾讯","type": 1,"api": "https://wabc.ml/mao/1.php/provide/vod/","searchable": 1,"quickSearch": 1,"filterable": 0},
{"key":"csp_appysv2_9e国语","name":"9e国语","type":3,"api":"csp_AppYsV2","searchable":1,"quickSearch":1,"filterable":1,"ext":"http://vod.9e03.com/lvdou_api.php/v1.vod"},

## XPath系列, APP影视规则, 资源网采集
{"key": "csp_xpath_2BT","name": "两个BT","type": 3,"api": "csp_XPathFilter","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/2BT.json"}, 
{"key": "csp_xpath_工人","name": "工人","type": 3,"api": "csp_XPathMacFilter","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/519-gongreng.json"},
{"key": "csp_xpath_在线","name": "在线","type": 3,"api": "csp_XPathMacFilter","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/zxzj.js"},
{"key": "csp_xpath_奇优","name": "奇优","type": 3,"api": "csp_XPath","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/603-qiyou.js"},
{"key": "csp_xpath_骚火","name": "骚火","type": 3,"api": "csp_XPath","searchable": 0,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/00-%E9%AA%9A%E7%81%AB.js?download=false"},
{"key": "csp_xpath_花猫","name": "花猫","type": 3,"api": "csp_XPathMacFilter","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/513-huamaotv.js?download=false"},
{"key": "csp_xpath_38蓝光","name": "38蓝光","type": 3,"api": "csp_XPathFilter","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/503-38lg.js?download=false"},
{"key": "csp_xpath_友播","name": "友播","type": 3,"api": "csp_XPath","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/503-yb.js?download=false"},
{"key": "csp_xpath_淘剧","name": "淘剧","type": 3,"api": "csp_XPathFilter","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/503-taojuya.js?download=false"},
{"key": "csp_xpath_厂长资源","name": "厂长资源","type": 3,"api": "csp_XPath","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/426-test-czzy.js?download=false"},
{"key": "csp_xpath_天天影视","name": "天天影视","type": 3,"api": "csp_XPathFilter","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/503-tiantianys.js?download=false"},	
{"key": "csp_xpath_小熊","name": "小熊","type": 3,"api": "csp_XPath","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/108-%E5%B0%8F%E7%86%8A%E5%BD%B1%E8%A7%86.js?download=false"},	
{"key": "csp_xpath_VIP1280","name": "VIP1280","type": 3,"api": "csp_XPath","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/101-vip1280.js?download=false"},
{"key": "csp_xpath_31看","name": "31看","type": 3,"api": "csp_XPath","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/11-31%E7%9C%8B.js?download=false"},
{"key": "csp_xpath_城市","name": "城市","type": 3,"api": "csp_XPathMac","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/112-xinnongm.json?download=false"},
{"key": "csp_xpath_看看","name": "看看","type": 3,"api": "csp_XPathMacFilter","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/516-wmkankan.js?download=false"},
{"key": "csp_xpath_九州","name": "九州","type": 3,"api": "csp_XPathMacFilter","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/unss.json?download=false"},
{"key": "csp_xpath_钉钉","name": "钉钉","type": 3,"api": "csp_XPath","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/72-dingzidy?download=false"},
{"key": "csp_xpath_兔子","name": "兔子","type": 3,"api": "csp_XPath","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/80-mozhachu?download=false"},
{"key": "csp_XPath_哔嘀","name": "哔嘀","type": 3,"api": "csp_XPathMacFilter","searchable": 1,"quickSearch": 1,"filterable": 1,"ext": "https://gitlab.com/al757/cc/-/raw/master/XPath/519-bidi.json"},

{"key":"360_spider","name":"🎾360(官源)","type":3,"api":"csp_SP360","searchable":1,"quickSearch":1,"filterable":1,"ext":"https://gitlab.com/al757/cc/-/raw/master/XPath/360.json"},	
//{"key":"sougou_spider","name":"🎾360(官源)","type":3,"api":"csp_SPSogou","searchable":1,"quickSearch":1,"filterable":1,"ext":"https://gitlab.com/al757/cc/-/raw/master/XPath/sougou.json"},	
{"key":"csp_SP2345","name":"🎾2345(SP)","type":3,"api":"csp_SP2345","searchable":1,"quickSearch":1,"filterable":1,"ext":""https://gitlab.com/al757/cc/-/raw/master/XPath/2345.json"},
{"key":"csp_SPSogou","name":"🎾Sougou(SP)","type":3,"api":"csp_SPSogou","searchable":1,"quickSearch":1,"filterable":1,"ext":""https://gitlab.com/al757/cc/-/raw/master/XPath/sougou.json"},

{"key":"csp_biubiu_斗鱼","name":"🥒斗鱼(XB)","type":3,"api":"csp_XBiubiu","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/Biubiu/斗鱼.json"},



{"key":"csp_xpath_huya","name":"虎牙(XPF)","type":3,"api":"csp_XPathMacFilter","searchable":1,"quickSearch":1,"filterable":1,"ext":"https://gitlab.com/al757/cc/-/raw/master/XPath/huya2.json"},
{"key":"csp_xpath_kuqimv","name":"酷奇MV(XP)","type":3,"api":"csp_XPath","searchable":0,"quickSearch":0,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/XPath/kuqimv.json"},
{"key":"csp_xpath_libv","name":"Libvio(XPF)","type":3,"api":"csp_XPathMacFilter","searchable":1,"quickSearch":1,"filterable":1,"ext":"https://gitlab.com/al757/cc/-/raw/master/XPath/libv.json"},
{"key":"csp_xpath_axx2","name":"爱西西影视(XPF)","type":3,"api":"csp_XPathFilter","searchable":1,"quickSearch":1,"filterable":1,"ext":"https://gitlab.com/al757/cc/-/raw/master/XPath/Aixixi.json"},
{"key":"csp_xpath_jpys","name":"极品影视(XPF)","type":3,"api":"csp_XPathMacFilter","searchable":1,"quickSearch":1,"filterable":1,"ext":"https://gitlab.com/al757/cc/-/raw/master/XPath/jpys.json"},
{"key":"csp_xpath_lranc","name":"天天影视(XPF)","type":3,"api":"csp_XPathMacFilter","searchable":1,"quickSearch":1,"filterable":1,"ext":"https://gitlab.com/al757/cc/-/raw/master/XPath/lranc.json"},
{"key":"csp_xpath_huohuo99","name":"火火影视(XP)","type":3,"api":"csp_XPath","searchable":1,"quickSearch":1,"filterable":1,"ext":"https://gitlab.com/al757/cc/-/raw/master/XPath/huohuo99.json"},
{"key":"csp_xpath_tjyy","name":"奇优影院(XP)","type":3,"api":"csp_XPathMac","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/XPath/huigutongying.json"},
{"key":"csp_xpath_duboku","name":"独播库(XPMac)","type":3,"api":"csp_XPathMac","searchable":1,"quickSearch":1,"filterable":0,"ext":"https://gitlab.com/al757/cc/-/raw/master/XPath/duboku2.json"},

//{"key":"ld","name":"尘落(ph)","type":1,"api":"http://pandown.pro/maoys/xuangz.php?ac=list","searchable":1,"quickSearch":1,"filterable":1},
{"key":"lz","name":"乐猪(ph)","type":1,"api":"http://pandown.pro/maoys/lezhu.php?ac=list","searchable":1,"quickSearch":1,"filterable":1},
//{"key":"cokemv","name":"Coke(ph)","type":1,"api":"http://pandown.pro/maoys/cokemv.php?ac=list","searchable":1,"quickSearch":1,"filterable":1},
//{"key":"libvio","name":"LIBVI(ph)","type":1,"api":"http://pandown.pro/maoys/libvio.php?ac=list","searchable":1,"quickSearch":1,"filterable":1},
{"key":"aytvip","name":"爱优腾(ph)","type":1,"api":"https://gfzycj.hnmj.vip/api.php/provide/vod/?ac=list","searchable":1,"quickSearch":1,"filterable":1},

#优质
{"key":"csp_appysv2_9e国语","name":"9e国语","type":3,"api":"csp_AppYsV2","searchable":1,"quickSearch":1,"filterable":1,"ext":"http://vod.9e03.com/lvdou_api.php/v1.vod"},
{"key":"csp_appysv2_益达影院","name":"益达影院(优)","type":3,"api":"csp_AppYsV2","searchable":1,"quickSearch":1,"filterable":1,"ext":"http://luobu.yss6080.com/mogai_api.php/v1.vod"},
{"key":"csp_appysv2_南府影视","name":"南府影视(优)","type":3,"api":"csp_AppYsV2","searchable":1,"quickSearch":1,"filterable":1,"ext":"http://iapp.nfuxs.club/nfuxs.php/v1.vod"},
{"key":"csp_appysv2_看看影视","name":"看看影视(优)","type":3,"api":"csp_AppYsV2","searchable":1,"quickSearch":1,"filterable":1,"ext":"http://kk.ccboke.top/ruifenglb_api.php/v1.vod"},
{"key":"csp_appysv2_飓风影院","name":"飓风影院(优)","type":3,"api":"csp_AppYsV2","searchable":1,"quickSearch":1,"filterable":1,"ext":"http://yidayy.top/lehailb_api.php/v1.vod"},
{"key":"csp_appysv2_段友影视","name":"段友影视(优)","type":3,"api":"csp_AppYsV2","searchable":1,"quickSearch":1,"filterable":1,"ext":"https://shangjihuoke.com/api.php/tv.vod"},
{"key":"csp_appysv2_段友影视2","name":"段友影视2(优)","type":3,"api":"csp_AppYsV2","searchable":1,"quickSearch":1,"filterable":1,"ext":"http://121.204.249.135:4433/ruifenglb_api.php/v1.vod"},
{"key":"csp_appysv2_300看世界","name":"300看世界(优)","type":3,"api":"csp_AppYsV2","searchable":1,"quickSearch":1,"filterable":1,"ext":"https://300ys.xyz/mogai_api.php/v1.vod"},
#

{"key":"8090资源","name":"8090资源","type":1,"api":"http://zy.yilans.net:8090/api.php/provide/vod/","playUrl":"https://www.8090.la/8090/?url=","searchable":1,"quickSearch":1},
{"key":"诺讯资源","name":"诺讯资源","type":1,"api":"http://caiji.nxflv.com/api.php/provide/vod/","playUrl":"https://www.nxflv.com/?url=","searchable":1,"quickSearch":1},
{"key":"英皇官采","name":"英皇官采","type":0,"api":"https://apicdn.vipm3u8.com/xml","playUrl":"https://player.cdn.ormanjiaju.com/player?token=4732bUERfVb60lWNSLrsd5-2s1r70KeA89C3VwrGYYdByboQT9o4OzxIr5-8/cX9-sO6&vid=","searchable":1,"quickSearch":1},
{"key":"江北资源","name":"江北资源","type":1,"api":"https://gfzycj.hnmj.vip/api.php/provide/vod/","playUrl":"parse:parwix1","searchable":1,"quickSearch":1},
{"key":"思古官采","name":"思古官采","type":1,"api":"http://zy.sgyun.me/api.php/provide/vod/","playUrl":"parse:parwix1","searchable":1,"quickSearch":1},
{"key":"星一官采","name":"星一官采","type":1,"api":"https://gcku.suboyun.vip/api.php/provide/vod/","playUrl":"https://www.xing1.vip/player/dp/?url=","searchable":1,"quickSearch":1},
{"key":"土狗官采","name":"土狗官采","type":1,"api":"http://vip-02.tgzy.cc/api.php/provide/vod/","playUrl":"","searchable":1,"quickSearch":1},
{"key":"极速官采","name":"极速官采","type":1,"api":"http://gc.zhuijuba.vip/api.php/provide/vod/","playUrl":"","searchable":1,"quickSearch":1},
{"key":"首涂官采","name":"首涂官采","type":1,"api":"http://zy.ishoutu.com/api.php/provide/vod/","playUrl":"","searchable":1,"quickSearch":1},
{"key":"雪人官采","name":"雪人官采","type":1,"api":"https://zl.chinafix.wang/api.php/provide/vod/","playUrl":"","searchable":1,"quickSearch":1},




{"key":"红牛资源","name":"红牛资源","type":1,"api":"https://www.hongniuzy2.com/api.php/provide/vod/","playUrl":"","searchable":1,"quickSearch":1},
{"key":"快播资源","name":"快播资源","type":1,"api":"http://www.kuaibozy.com/api.php/provide/vod/","playUrl":"","searchable":1,"quickSearch":1},
{"key":"八戒资源","name":"八戒资源","type":1,"api":"http://cj.bajiecaiji.com/inc/apijson_vod.php","playUrl":"","searchable":0,"quickSearch":0},


{"key":"293影视","name":"293影视","type":1,"api":"http://293x.yhzy.xyz/mv/api.php/provide/vod/","playUrl":"","searchable":1,"quickSearch":1},
{"key":"追剧吧","name":"追剧吧","type":1,"api":"http://zhuiju8.vip/api.php/provide/vod/","playUrl":"","searchable":1,"quickSearch":1},
{"key":"每天看看","name":"每天看看","type":1,"api":"http://47.113.126.237:1234/api.php/provide/vod/","playUrl":"parse:parwix1","searchable":1,"quickSearch":1},
{"key":"畅视影视","name":"畅视影视","type":1,"api":"http://app.reboju.net/api.php/provide/vod/","playUrl":"parse:parwix1","searchable":1,"quickSearch":1},


{"key":"爱看","name":"爱看","type":1,"api":"http://tvcms.ikan6.vip/api.php/provide/vod/","playUrl":"","searchable":1,"quickSearch":1},
//{"key":"看365","name":"看365","type":1,"api":"https://www.kan365.xyz/api.php/provide/vod/","playUrl":"","searchable":1,"quickSearch":1},
{"key":"csp_appysv2_看365","name":"看365","type":3,"api":"csp_AppYsV2","searchable":1,"quickSearch":1,"filterable":1,"ext":"https://www.kan365.xyz/api.php/app/"},
#
// 推送功能扩展（需v2.1.0.Beta6及以上版本）
// 需要在配置文件中，加入key为push_agent的站点，数据返回格式和普通爬虫一致
// 支持推送播放网盘链接
// https://github.com/catvod/CatVodTVSpider
//{"key":"push_agent","name":"推送","type":3,"api":"csp_PushAgent","searchable":0,"quickSearch":0,"filterable":0}
{"key":"push_agent","name":"推送","type":3,"api":"csp_Ali","searchable":0,"quickSearch":0,"filterable":0}
#
],

"parses":[
{"name":"解析聚合","type":3,"url":"Demo"},
{"name":"Json并发","type":2,"url":"Parallel"},
{"name":"Json轮询","type":2,"url":"Sequence"},
# type1 聚合.并发.轮询
//{"name":"293","type":1,"url":"http://81.71.48.249:4456/jsonc/longxia.php?url=","ext":{"flag":["qq","qiyi","mgtv","youku","letv","sohu","xigua","1905"],"header":{"User-Agent":"Dart/2.14 (dart:io)"}}},
{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视","type":1,"url":"https://json.5lp.net/json.php?url="},
{"name": "线路NX",	"url": "https://vip.nxflv.com/home/api?uid=701203&key=bltuyCEFKLTX013679&url=",	"type": 1,	"i": "77","ext": {"flag": ["qiyi", "爱奇艺", "奇艺", "qq", "腾讯", "youku", "优酷", "pptv", "PPTV", "letv", "乐视", "bilibili", "哔哩哔哩", "哔哩", "mgtv", "芒果"]}},
{"name":"线路1","type":1,"url":"http://27.124.4.42:4567/jhjson/ceshi.php?url=","ext":{"flag":["qiyi","qq","letv","sohu","youku","mgtv","bilibili","wasu","xigua","1905"]}},	
{"name":"线路2","url": "https://jhjx.kuanjv.com/newky/?url=","type": 1,"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","pptv","PPTV","letv","乐视","bilibili","哔哩哔哩","哔哩","mgtv","芒果"]}},
{"name":"线路3","url": "https://jie.1z1.cc/api/?key=HdMmTMfyf1uTOQUL0b&url=","type": 1,"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","pptv","PPTV","letv","乐视","bilibili","哔哩哔哩","哔哩","mgtv","芒果"]}},
{"name":"线路4","url": "http://110.42.2.115:880/analysis/json/?uid=2233&my=eginqstBCJMNSUX689&url=","type": 1,"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","pptv","PPTV","letv","乐视","bilibili","哔哩哔哩","哔哩","mgtv","芒果"]}},	
{"name":"线路5","url": "http://110.42.2.115:880/analysis/json/?uid=2233&my=eginqstBCJMNSUX689&url=","type": 1,"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","pptv","PPTV","letv","乐视","bilibili","哔哩哔哩","哔哩","mgtv","芒果"]}},	
{"name":"线路6","url": "http://27.124.4.42:4567/jhjson/ceshi.php?url=","type": 1,"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","pptv","PPTV","letv","乐视","bilibili","哔哩哔哩","哔哩","mgtv","芒果"]}},	

	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视2","type":1,"url":"https://json.pangujiexi.com:12345/json.php?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视3","type":1,"url":"https://jx.hfyrw.com/mao.go?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视4","type":1,"url":"http://api.vip123kan.vip/?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视5","type":1,"url":"https://lt.kpjx.cc/Mao.php?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视6","type":1,"url":"https://jx.mczdyw.com/xg.php?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视7","type":1,"url":"http://admin.vodjx.top/json.php?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视8","type":1,"url":"https://vip.2ktvb.com/play/parse.php?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视9","type":1,"url":"https://app.iminna.com/jx/?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视10","type":1,"url":"https://humaosp.com/json.php?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视11","type":1,"url":"https://jhjx.kuanjv.com/newky/?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视12","type":1,"url":"https://admin.4kjx.top/json.php?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视13","type":1,"url":"https://www.2g88.vip/mgyy.php?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视14","type":1,"url":"http://119.4.70.117:9095/jhjx.php?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视15","type":1,"url":"https://jiexi.300ys.xyz/json/jianghujx.php/?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视16","type":1,"url":"http://jiexi.300ys.xyz/json/mm3u8.php/?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视17","type":1,"url":"https://www.shangjihuoke.com/json.php/?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视18","type":1,"url":"https://jx.zhanlangbu.com/json.php/?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视19","type":1,"url":"https://aa.xkys.tv/json.php?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视20","type":1,"url":"https://jx.1080p.icu:4597/svip00.php?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视21","type":1,"url":"https://17.yxq.email/e4a/?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视22","type":1,"url":"https://json.3316.ltd/?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视23","type":1,"url":"https://jx.hfyrw.com/mao.go?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视24","type":1,"url":"http://103.40.246.5:6881/MmAI00o1yun218/?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视25","type":1,"url":"http://27.124.4.42:4567/jhjson/ceshi.php?url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视26","type":1,"url":"http://vip.nicefamily.club/api/?key=vWH5RAvFG7cKeOgdCY&url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视27","type":1,"url":"http://ck.laobandq.com/3515240842.php?pltfrom=1100&url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视28","type":1,"url":"https://ldy.jx.cn/wp-api/getvodurl.php?token=1001&vid="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视29","type":1,"url":"http://cs.91dmmm.cc/api/?key=WoCtDWXQ8Wbmk5v23Y&url="},
	{"ext":{"flag":["qiyi","爱奇艺","奇艺","qq","腾讯","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","sohu","搜狐","wuduzy"]},"t":612,"name":"艾特影视30","type":1,"url":"https://jf.96ym.cn/home/api?type=ys&uid=1319830&key=cefgnoprtvxyzBGKP6&url="},
{"name":"江湖解析1","type":1,"url":"http://103.40.240.46/jh/?url=","ext":{"flag":["renrenmi","qq","腾讯","youku","优酷","mgtv","芒果","xigua","西瓜"]}},
{"name":"江湖解析2","type":1,"url":"https://cs.miaot.top/api/?key=BSY37DYJdYHMzdh7YT&url=","ext":{"flag":["renrenmi","qq","腾讯","youku","优酷","mgtv","芒果","xigua","西瓜"]}},
{"name":"江湖解析3","type":1,"url":"https://www.zhongguogps.com/jiexi/api1.php?url=","ext":{"flag":["renrenmi","qq","腾讯","youku","优酷","mgtv","芒果","xigua","西瓜"]}},
{"name":"江湖解析4","type":1,"url":"https://svip.96ad.cn/api/?key=1QBTj74miyQbFVX7LR&url=","ext":{"flag":["renrenmi","qq","腾讯","youku","优酷","mgtv","芒果","xigua","西瓜"]}},

    {
      "name": "解析28ltnb",
      "type": 1,
      "url": "https://languangyingshiziyuan.1080zy.top/longtengzy.php/?url=",
      "ext": {
        "flag": [
          "ltnb"
        ]
      }
    },
    {
      "name": "解析29xfyun",
      "type": 1,
      "url": "https://vip.xfyun.one/home/api?type=ys&uid=2581923&key=ceijpquvBMOSUVXZ23&url=",
      "ext": {
        "flag": [
          "xfyun"
        ]
      }
    },
    {
      "name": "解析30wuduzy",
      "type": 1,
      "url": "https://aa.xkys.tv/json.php?url=",
      "ext": {
        "flag": [
          "wuduzy"
        ]
      }
    },
    {
      "name": "解析31rx",
      "type": 1,
      "url": "https://svip.spchat.top/api/?type=ys&key=bKemW41JnxmQb4l67h&url=",
      "ext": {
        "flag": [
          "rx"
        ]
      }
    },
    {
      "name": "解析32ltnb",
      "type": 1,
      "url": "https://vip.aiaine.com/api/?key=fOWaGgFU45zlIjvbHI&url=",
      "ext": {
        "flag": [
          "ltnb",
          "renrenmi"
        ]
      }
    },
	
# type0 手动解析
{"name":"江湖解析5","type":0,"url":"https://cache.tegouys.com/dwsm.php?url=","ext":{"flag":["wuduzy","qq","腾讯","youku","优酷","mgtv","芒果","xigua","tkqp"]}},
{"name":"OJBK","type":0,"url":"https://jmwl.qd234.cn/v/?v=","ext":{"flag":["ltnb","renrenmi","rx","xfyun","muxm3u8","xigua","xueren","qq","腾讯","qiyi","爱奇艺","奇艺","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩","pptv","PPTV","sohu","letv"]}},
{"name":"M117","type":0,"url":"http://1.117.152.239:39000/?url="},
{"name":"zui","type":0,"url":"https://jx.zui.cm/?url=","ext":{"flag":["ltnb"]}},
{"name":"parwix1","type":0,"url":"https://jx.parwix.com:4433/player/?url=","ext":{"flag":["qq","腾讯","qiyi","爱奇艺","奇艺","youku","优酷","mgtv","芒果","letv","乐视","pptv","PPTV","sohu","bilibili","哔哩哔哩","哔哩"]}},
{"name":"parwix2","type":0,"url":"https://jx.parwix.com:4433/player/analysis.php?v=","ext":{"flag":["qq","腾讯","qiyi","爱奇艺","奇艺","youku","优酷","mgtv","芒果","letv","乐视","pptv","PPTV","sohu","bilibili","哔哩哔哩","哔哩"]}},
{"name":"xuerenweb","type":0,"url":"https://s.2tu.uk/?url=","ext":{"flag":["xueren"]}},
{"name":"xuerenweb2","type":0,"url":"https://xrm3u8.qd234.cn/?url=","ext":{"flag":["xueren"]}},
{"name":"美剧虫","type":0,"url":"https://jx.daiguaji.com/?url=","ext":{"flag":["zijian"]}},
{"name":"miao","type":0,"url":"https://jx.58g8.com/1/?url=","ext":{"flag":["miaoparty"]}},
{"name":"web1","type":0,"url":"https://www.nxflv.com/?url=","ext":{"flag":["youku","优酷","mgtv","芒果","qq","腾讯","qiyi","爱奇艺","qq","奇艺","sohu","letv"]}},
{"name":"万能解析","type":0,"url":"https://vip.legendwhb.cn/m3u8.php?url=","ext":{"flag":["ltnb","renrenmi","qq","腾讯","qiyi","爱奇艺","奇艺","youku","优酷","mgtv","芒果","bilibili","哔哩哔哩","哔哩"]}},
{"name":"ltnb04","type":0,"url":"https://vip.bljiex.com/?v=","ext":{"flag":["ltnb"]}},
{"name":"ltnb02","type":0,"url":"https://jx.zui.cm/?url=","ext":{"flag":["ltnb"]}},
{"name":"CL4K01","type":0,"url":"https://ys.ling00.cn/CL4K/?url=","ext":{"flag":["CL4K","qq","腾讯"]}},
{"name":"CL4K02","type":0,"url":"https://app.okmedcos.com/4k/?url=","ext":{"flag":["CL4K","qq","腾讯","pptv","PPTV"]}}
],
"flags":["youku","qq","iqiyi","qiyi","letv","sohu","tudou","pptv","mgtv","wasu","bilibili","renrenmi","优酷","芒果","腾讯","爱奇艺","奇艺","ltnb","rx","CL4K","xfyun","wuduzy"],
"ijk":[
{"group":"软解码","options":[
{"category":4,"name":"opensles","value":"0"},
{"category":4,"name":"overlay-format","value":"842225234"},
{"category":4,"name":"framedrop","value":"1"},
{"category":4,"name":"soundtouch","value":"1"},
{"category":4,"name":"start-on-prepared","value":"1"},
{"category":1,"name":"http-detect-range-support","value":"0"},
{"category":1,"name":"fflags","value":"fastseek"},
{"category":2,"name":"skip_loop_filter","value":"48"},
{"category":4,"name":"reconnect","value":"1"},
{"category":4,"name":"enable-accurate-seek","value":"0"},
{"category":4,"name":"mediacodec","value":"0"},
{"category":4,"name":"mediacodec-auto-rotate","value":"0"},
{"category":4,"name":"mediacodec-handle-resolution-change","value":"0"},
{"category":4,"name":"mediacodec-hevc","value":"0"},
{"category":1,"name":"dns_cache_timeout","value":"600000000"}
]},
{"group":"硬解码","options":[
{"category":4,"name":"opensles","value":"0"},
{"category":4,"name":"overlay-format","value":"842225234"},
{"category":4,"name":"framedrop","value":"1"},
{"category":4,"name":"soundtouch","value":"1"},
{"category":4,"name":"start-on-prepared","value":"1"},
{"category":1,"name":"http-detect-range-support","value":"0"},
{"category":1,"name":"fflags","value":"fastseek"},
{"category":2,"name":"skip_loop_filter","value":"48"},
{"category":4,"name":"reconnect","value":"1"},
{"category":4,"name":"enable-accurate-seek","value":"0"},
{"category":4,"name":"mediacodec","value":"1"},
{"category":4,"name":"mediacodec-auto-rotate","value":"1"},
{"category":4,"name":"mediacodec-handle-resolution-change","value":"1"},
{"category":4,"name":"mediacodec-hevc","value":"1"},
{"category":1,"name":"dns_cache_timeout","value":"600000000"}
]}],
"ads":[
"mimg.0c1q0l.cn",
"www.googletagmanager.com",
"www.google-analytics.com",
"mc.usihnbcq.cn",
"mg.g1mm3d.cn",
"mscs.svaeuzh.cn",
"cnzz.hhttm.top",
"tp.vinuxhome.com",
"cnzz.mmstat.com",
"www.baihuillq.com",
"s23.cnzz.com",
"z3.cnzz.com",
"c.cnzz.com",
"stj.v1vo.top",
"z12.cnzz.com",
"img.mosflower.cn",
"tips.gamevvip.com",
"ehwe.yhdtns.com",
"xdn.cqqc3.com",
"www.jixunkyy.cn",
"sp.chemacid.cn",
"hm.baidu.com",
"s9.cnzz.com",
"z6.cnzz.com",
"um.cavuc.com",
"mav.mavuz.com",
"wofwk.aoidf3.com",
"z5.cnzz.com",
"xc.hubeijieshikj.cn",
"tj.tianwenhu.com",
"xg.gars57.cn",
"k.jinxiuzhilv.com",
"cdn.bootcss.com",
"ppl.xunzhuo123.com",
"xomk.jiangjunmh.top",
"img.xunzhuo123.com",
"z1.cnzz.com",
"s13.cnzz.com",
"xg.huataisangao.cn",
"z7.cnzz.com",
"xg.huataisangao.cn",
"z2.cnzz.com",
"s96.cnzz.com",
"q11.cnzz.com",
"thy.dacedsfa.cn",
"xg.whsbpw.cn",
"s19.cnzz.com",
"z8.cnzz.com",
"s4.cnzz.com",
"f5w.as12df.top",
"ae01.alicdn.com",
"www.92424.cn",
"k.wudejia.com",
"vivovip.mmszxc.top",
"qiu.xixiqiu.com",
"cdnjs.hnfenxun.com",
"cms.qdwght.com"
]
}
